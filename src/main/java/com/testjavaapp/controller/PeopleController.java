package com.testjavaapp.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.testjavaapp.dto.PeopleDto;
import com.testjavaapp.dto.PeopleInputDto;
import com.testjavaapp.mapper.inputDtoToModel.PeopleInputDtoToModelMapper;
import com.testjavaapp.mapper.modelToDto.PeopleModelToDtoMapper;
import com.testjavaapp.model.People;
import com.testjavaapp.repository.PeopleRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/v1")
public class PeopleController {

	@Autowired
	private PeopleRepository peopleRepository;

	@Autowired
	private PeopleInputDtoToModelMapper peopleInputDtoToModelMapper;
	@Autowired
	private PeopleModelToDtoMapper peopleModelToDtoMapper;

	@ApiOperation(value = "Retorna uma lista de informações de pessoas")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Retorna a lista de dados de pessoas"),
        @ApiResponse(code = 403, message = "sem permissão para executar a ação"),
        @ApiResponse(code = 500, message = "Erro inesperado"),
    })
	@RequestMapping(value = "/peoples", method = RequestMethod.GET, produces="application/json")
	public Page<PeopleDto> getAllPeoples(Pageable pageable) {
		return peopleRepository.findAll(pageable).map(people -> peopleModelToDtoMapper.toMap(people));
	}
	
    @ApiOperation(value = "Recupera as informação da pessoa")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Retorna os dados da pessoa"),
        @ApiResponse(code = 403, message = "sem permissão para executar a ação"),
        @ApiResponse(code = 404, message = "Pessoa não encontrada"),
    })
	@RequestMapping(value = "/people/{id}", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> retrievePeople(@PathVariable(value = "id") long id) {
		return peopleRepository.findById(id).map(people -> {
			PeopleDto dto = peopleModelToDtoMapper.toMap(people);
		
			return  ResponseEntity.ok(dto);
		}).orElse(ResponseEntity.notFound().build());
	}

    @ApiOperation(value = "Atualiza as informações da pessoa")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Pessoa adicionada com sucesso"),
        @ApiResponse(code = 403, message = "sem permissão para executar a ação"),
    })
	@RequestMapping(value = "/people", method =  RequestMethod.POST, produces="application/json", consumes="application/json")
	public ResponseEntity<?> createPeople(@Valid @RequestBody PeopleInputDto peopleDto) {
		People people = peopleInputDtoToModelMapper.toMap(peopleDto);

		people = peopleRepository.save(people);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest()
												  .path("/{id}")
												  .buildAndExpand(people.getId())
												  .toUri();

		return ResponseEntity.created(location).build();
	}

    @ApiOperation(value = "Atualiza as informações da pessoa")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Pessoa atualizada com sucesso"),
        @ApiResponse(code = 403, message = "sem permissão para executar a ação"),
        @ApiResponse(code = 404, message = "Pessoa não encontrada"),
    })
	@RequestMapping(value = "/people/{id}", method =  RequestMethod.PUT, produces="application/json", consumes="application/json")
	public ResponseEntity<?> updatePeople(@PathVariable(value = "id") long id, @Valid @RequestBody PeopleInputDto peopleDto) {
		return peopleRepository.findById(id).map(people -> {
			people.setName(peopleDto.getName());
			people.setDocument(peopleDto.getDocument());
			people.setBornDate(peopleDto.getBornDate());
			
			peopleRepository.save(people);

			return ResponseEntity.noContent().build();
		}).orElse(ResponseEntity.notFound().build());
	}

    @ApiOperation(value = "Remove as informação da pessoa")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Pessoa removida com sucesso"),
        @ApiResponse(code = 403, message = "sem permissão para executar a ação"),
        @ApiResponse(code = 404, message = "Pessoa não encontrada"),
    })
	@RequestMapping(value = "/people/{id}", method = RequestMethod.DELETE, produces="application/json")
	public ResponseEntity<?> deletePeople(@PathVariable(value = "id") long id)
	{
		return peopleRepository.findById(id).map(people -> {
			peopleRepository.deleteById(people.getId());
			
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}
}