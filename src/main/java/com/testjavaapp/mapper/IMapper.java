package com.testjavaapp.mapper;

import java.util.List;

public interface IMapper<TSource, TDestiny> {
	TDestiny toMap(TSource obj);

	List<TDestiny> toMap(List<TSource> list);
}