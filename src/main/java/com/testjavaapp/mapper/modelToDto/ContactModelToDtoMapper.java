package com.testjavaapp.mapper.modelToDto;

import org.springframework.stereotype.Component;

import com.testjavaapp.dto.ContactDto;
import com.testjavaapp.mapper.BaseMapper;
import com.testjavaapp.model.Contact;

@Component
public class ContactModelToDtoMapper extends BaseMapper<Contact, ContactDto> {

	@Override
	public ContactDto toMap(Contact obj) {
		if (obj == null)
			return null;

		return modelMapper.map(obj, ContactDto.class);
	}
}