package com.testjavaapp.mapper.modelToDto;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.testjavaapp.dto.PeopleDto;
import com.testjavaapp.mapper.IMapper;
import com.testjavaapp.model.People;

@Component
public class PeopleModelToDtoMapper implements IMapper<People, PeopleDto> {

	private final ModelMapper modelMapper = new ModelMapper();

	@Override
	public PeopleDto toMap(People obj) {
		if (obj == null)
			return null;

		return modelMapper.map(obj, PeopleDto.class);
	}

	public List<PeopleDto> toMap(List<People> list) {
		if (list == null)
			return null;

		return list.stream().map(obj -> toMap(obj)).collect(Collectors.toList());
	}
}