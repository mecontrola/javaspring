package com.testjavaapp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

public abstract class BaseMapper<TSource, TDestiny> implements IMapper<TSource, TDestiny> {

	protected final ModelMapper modelMapper = new ModelMapper();

	public abstract TDestiny toMap(TSource obj);

	public List<TDestiny> toMap(List<TSource> list) {
		if (list == null)
			return null;

		return list.stream().map(obj -> toMap(obj)).collect(Collectors.toList());
	}
}