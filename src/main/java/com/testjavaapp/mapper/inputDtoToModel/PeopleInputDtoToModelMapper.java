package com.testjavaapp.mapper.inputDtoToModel;

import org.springframework.stereotype.Component;

import com.testjavaapp.dto.PeopleInputDto;
import com.testjavaapp.mapper.BaseMapper;
import com.testjavaapp.model.People;

@Component
public class PeopleInputDtoToModelMapper extends BaseMapper<PeopleInputDto, People> {

	@Override
	public People toMap(PeopleInputDto obj) {
		if (obj == null)
			return null;

		return modelMapper.map(obj, People.class);
	}
}