package com.testjavaapp.mapper.inputDtoToModel;

import org.springframework.stereotype.Component;

import com.testjavaapp.dto.ContactInputDto;
import com.testjavaapp.mapper.BaseMapper;
import com.testjavaapp.model.Contact;

@Component
public class ContactInputDtoToModelMapper extends BaseMapper<ContactInputDto, Contact> {

	@Override
	public Contact toMap(ContactInputDto obj) {
		if (obj == null)
			return null;

		return modelMapper.map(obj, Contact.class);
	}
}