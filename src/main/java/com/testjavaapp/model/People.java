package com.testjavaapp.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tb_people")
public class People {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotBlank
	@Size(max = 100)
	@Column(nullable = false)
	private String name;

	@NotBlank
	@Size(max = 14)
	@Column(nullable = false, unique = true)
	private String document;

	@NotBlank
	@Column(nullable = false)
	private Date bornDate;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "contact_id")
	private List<Contact> contacts;

	public People() {
		super();

		contacts = new ArrayList<Contact>();
	}

	public long getId() {
		return id;
	}

	public People setId(long id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public People setName(String name) {
		this.name = name;
		return this;
	}

	public String getDocument() {
		return document;
	}

	public People setDocument(String document) {
		this.document = document;
		return this;
	}

	public Date getBornDate() {
		return bornDate;
	}

	public People setBornDate(Date bornDate) {
		this.bornDate = bornDate;
		return this;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public People setContacts(List<Contact> contacts) {
		this.contacts = contacts;
		return this;
	}

	public People addContact(Contact contact) {
		this.contacts.add(contact);
		return this;
	}
}