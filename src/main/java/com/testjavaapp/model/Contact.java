package com.testjavaapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "tb_contact")
public class Contact {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotBlank
	@Size(max = 150)
	@Column(nullable = false, length = 150)
	private String name;

	@NotBlank
	@Pattern(regexp = "^\\(\\d{2}\\)\\s\\d{4}\\-\\d{4}$")
	@Column(nullable = false, length = 14)
	private String phone;

	@NotBlank
	@Email
	@Size(min = 10, max = 150)
	@Column(nullable = false, length = 150)
	private String email;

	public Contact() {
		super();
	}

	public long getId() {
		return id;
	}

	public Contact setId(long id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public Contact setName(String name) {
		this.name = name;
		return this;
	}

	public String getPhone() {
		return phone;
	}

	public Contact setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public Contact setEmail(String email) {
		this.email = email;
		return this;
	}
}