package com.testjavaapp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.testjavaapp.model.People;

@Repository
public interface PeopleRepository extends JpaRepository<People, Long> {
	Page<People> findById(Long id, Pageable pageable);
}