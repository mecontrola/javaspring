package com.testjavaapp.dto;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class PeopleDto {

	@ApiModelProperty(value = "Código da pessoa")
	private long id;

	@ApiModelProperty(value = "Nome da pessoa")
	private String name;

	@ApiModelProperty(value = "Documento da pessoa")
	private String document;

	@ApiModelProperty(value = "Data de nascimento da pessoa")
	private Date bornDate;

	@ApiModelProperty(value = "Lista de contatos da pessoa")
	private List<ContactDto> contacts;

	public long getId() {
		return id;
	}

	public PeopleDto setId(long id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public PeopleDto setName(String name) {
		this.name = name;
		return this;
	}

	public String getDocument() {
		return document;
	}

	public PeopleDto setDocument(String document) {
		this.document = document;
		return this;
	}

	public Date getBornDate() {
		return bornDate;
	}

	public PeopleDto setBornDate(Date bornDate) {
		this.bornDate = bornDate;
		return this;
	}

	public List<ContactDto> getContacts() {
		return contacts;
	}

	public PeopleDto setContacts(List<ContactDto> contacts) {
		this.contacts = contacts;
		return this;
	}

	public PeopleDto addContacts(ContactDto contact) {
		this.contacts.add(contact);
		return this;
	}
}