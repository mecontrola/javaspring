package com.testjavaapp.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

public class ContactInputDto {

	@NotBlank
	@Size(max = 150)
	@ApiModelProperty(value = "Nome do contato")
	private String name;

	@NotBlank
	@Pattern(regexp = "^\\(\\d{2}\\)\\s\\d{4}\\-\\d{4}$")
	@ApiModelProperty(value = "Telefone do contato")
	private String phone;

	@NotBlank
	@Email
	@Size(min = 10, max = 150)
	@ApiModelProperty(value = "E-mail do contato")
	private String email;

	public String getName() {
		return name;
	}

	public ContactInputDto setName(String name) {
		this.name = name;
		return this;
	}

	public String getPhone() {
		return phone;
	}

	public ContactInputDto setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public ContactInputDto setEmail(String email) {
		this.email = email;
		return this;
	}
}