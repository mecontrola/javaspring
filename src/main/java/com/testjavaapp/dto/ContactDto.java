package com.testjavaapp.dto;

import io.swagger.annotations.ApiModelProperty;

public class ContactDto {

	@ApiModelProperty(value = "Código do contato")
	private long id;

	@ApiModelProperty(value = "Nome do contato")
	private String name;

	@ApiModelProperty(value = "Telefone do contato")
	private String phone;

	@ApiModelProperty(value = "E-mail do contato")
	private String email;

	public long getId() {
		return id;
	}

	public ContactDto setId(long id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public ContactDto setName(String name) {
		this.name = name;
		return this;
	}

	public String getPhone() {
		return phone;
	}

	public ContactDto setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public ContactDto setEmail(String email) {
		this.email = email;
		return this;
	}
}