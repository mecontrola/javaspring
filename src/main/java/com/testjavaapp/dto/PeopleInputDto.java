package com.testjavaapp.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.testjavaapp.validation.validator.CPF;
import com.testjavaapp.validation.validator.DateBigger;

import io.swagger.annotations.ApiModelProperty;

public class PeopleInputDto {

	@NotBlank
	@Size(max = 150)
	@ApiModelProperty(value = "Nome da pessoa")
	private String name;

	@NotBlank
	@CPF
	@ApiModelProperty(value = "Documento da pessoa")
	private String document;

	@NotNull
	@DateBigger
	@ApiModelProperty(value = "Data de nascimento da pessoa")
	private Date bornDate;

	@NotNull
	@Size(min = 1, message = "A contact must be informed")
	@ApiModelProperty(value = "Lista de contatos da pessoa")
	private List<ContactInputDto> contacts;

	public PeopleInputDto() {
		contacts = new ArrayList<ContactInputDto>();
	}

	public String getName() {
		return name;
	}

	public PeopleInputDto setName(String name) {
		this.name = name;
		return this;
	}

	public String getDocument() {
		return document;
	}

	public PeopleInputDto setDocument(String document) {
		this.document = document;
		return this;
	}

	public Date getBornDate() {
		return bornDate;
	}

	public PeopleInputDto setBornDate(Date bornDate) {
		this.bornDate = bornDate;
		return this;
	}

	public List<ContactInputDto> getContacts() {
		return contacts;
	}

	public PeopleInputDto setContacts(List<ContactInputDto> contacts) {
		this.contacts = contacts;
		return this;
	}

	public PeopleInputDto addContact(ContactInputDto contact) {
		this.contacts.add(contact);
		return this;
	}
}