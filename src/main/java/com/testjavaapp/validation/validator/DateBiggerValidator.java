package com.testjavaapp.validation.validator;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateBiggerValidator implements ConstraintValidator<DateBigger, Date> {
	@Override
	public boolean isValid(Date value, ConstraintValidatorContext context) {
		return new Date().after(value);
	}
}