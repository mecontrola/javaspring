package com.testjavaapp.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.testjavaapp.validation.CPFValidation;

public class CPFValidator implements ConstraintValidator<CPF, String> {

	private static final CPFValidation validator = new CPFValidation();

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return validator.isValid(value);
	}
}