package com.testjavaapp.validation;

public class CPFValidation {

	public boolean isValid(String value) {
		value = getOnlyNumbers(value);

		if (value.length() != 11)
			return false;

		if (hasAllRepeatedDigits(value))
			return false;

		int sum = getSumMultipliedByDecreasedFactor(value.substring(0, 9), 10);
		int mod = getMod11(sum);

		if (!isEqualVerifyingDigit(value, 9, mod))
			return false;

		sum = getSumMultipliedByDecreasedFactor(value.substring(0, 10), 11);
		mod = getMod11(sum);

		return isEqualVerifyingDigit(value, 10, mod);
	}

	private String getOnlyNumbers(String value) {
		return value.replaceAll("[^0-9]", "");
	}

	private boolean hasAllRepeatedDigits(String value) {
		char firstChar = value.charAt(0);

		for (int i = 1, l = value.length(); i < l; i++)
			if (firstChar != value.charAt(i))
				return false;

		return true;
	}

	private int getSumMultipliedByDecreasedFactor(String value, int factor) {
		int sum = 0;

		for (int i = 0, l = value.length(); i < l; i++)
			sum += Character.getNumericValue(value.charAt(i)) * (factor - i);

		return sum;
	}

	private int getMod11(int value) {
		int mod = 11 - (value % 11);

		if (mod > 9)
			return 0;

		return mod;
	}

	private boolean isEqualVerifyingDigit(String value, int posDigit, int mod) {
		return Character.getNumericValue(value.charAt(posDigit)) == mod;
	}
}