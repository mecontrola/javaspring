package com.testjavaapp.helper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import com.testjavaapp.dto.ContactDto;
import com.testjavaapp.dto.ContactInputDto;
import com.testjavaapp.dto.PeopleDto;
import com.testjavaapp.dto.PeopleInputDto;
import com.testjavaapp.model.Contact;
import com.testjavaapp.model.People;

public class AssertHelper {

	public static void assertEqualsListPeopleDto(List<PeopleDto> expected, List<People> actual) {
		assertEquals(expected.size(), actual.size());
		assertEqualsPeople(expected.get(0), actual.get(0));
	}

	public static void assertEqualsListPeople(List<People> expected, List<PeopleInputDto> actual) {
		assertEquals(expected.size(), actual.size());
		assertEqualsPeople(expected.get(0), actual.get(0));
	}

	public static void assertEqualsPeople(PeopleDto expected, People actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getDocument(), actual.getDocument());
		assertEquals(expected.getBornDate(), actual.getBornDate());
		assertEquals(expected.getContacts().size(), actual.getContacts().size());
		assertEqualsContact(expected.getContacts().get(0), actual.getContacts().get(0));
	}

	public static void assertEqualsPeople(People expected, PeopleInputDto actual) {
		assertEquals(expected.getId(), 0);
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getDocument(), actual.getDocument());
		assertEquals(expected.getBornDate(), actual.getBornDate());
		assertEquals(expected.getContacts().size(), actual.getContacts().size());
		assertEqualsContact(expected.getContacts().get(0), actual.getContacts().get(0));
	}

	public static void assertEqualsListContactDto(List<ContactDto> expected, List<Contact> actual) {
		assertEquals(expected.size(), actual.size());
		assertEqualsContact(expected.get(0), actual.get(0));
	}

	public static void assertEqualsListContact(List<Contact> expected, List<ContactInputDto> actual) {
		assertEquals(expected.size(), actual.size());
		assertEqualsContact(expected.get(0), actual.get(0));
	}

	public static void assertEqualsContact(ContactDto expected, Contact actual) {
		assertEquals(expected.getId(), actual.getId());
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getEmail(), actual.getEmail());
		assertEquals(expected.getPhone(), actual.getPhone());
	}

	public static void assertEqualsContact(Contact expected, ContactInputDto actual) {
		assertEquals(expected.getId(), 0);
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getEmail(), actual.getEmail());
		assertEquals(expected.getPhone(), actual.getPhone());
	}
}