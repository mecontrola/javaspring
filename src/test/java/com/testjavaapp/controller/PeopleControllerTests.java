package com.testjavaapp.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.testjavaapp.RestAPI;
import com.testjavaapp.dto.PeopleInputDto;
import com.testjavaapp.mock.data.PeopleData;
import com.testjavaapp.mock.dto.PeopleInputDtoMock;
import com.testjavaapp.mock.model.PeopleMock;
import com.testjavaapp.model.People;
import com.testjavaapp.repository.PeopleRepository;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {RestAPI.class})
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = { RestAPI.class })
public class PeopleControllerTests {

	private static final String BASE_URL = "/api/v1";
	
	private static final String BASE_URL_FULL = "http://localhost" + BASE_URL;
	
	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@MockBean
	private PeopleRepository peopleRepository;

	@BeforeEach
	void setup() {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	@DisplayName("[PeopleController.createPeople] Must be inserted a new people.")
	public void postPeople() throws Exception {

		when(peopleRepository.save(any(People.class))).thenReturn(PeopleMock.CreatePeople2());

		String json = getJSON(PeopleInputDtoMock.CreatePeople2());

		mockMvc.perform(post(BASE_URL + "/people")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(header().string("Location", BASE_URL_FULL + "/people/" + PeopleData.ID_PEOPLE_2));

	}

	@Test
	@DisplayName("[PeopleController.updatePeople] Must be updated a new people.")
	public void putPeople() throws Exception {
		when(peopleRepository.findById(eq(PeopleData.ID_PEOPLE_2))).thenReturn(Optional.of(PeopleMock.CreatePeople2()));
		when(peopleRepository.save(any(People.class))).thenReturn(PeopleMock.CreatePeople2());

		String json = getJSON(PeopleInputDtoMock.CreatePeople2());

		mockMvc.perform(put(BASE_URL + "/people/" + PeopleData.ID_PEOPLE_2)
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNoContent());

	}

	@Test
	@DisplayName("[PeopleController.deletePeople] Must be deleted a new people.")
	public void deletePeople() throws Exception {
		when(peopleRepository.findById(eq(PeopleData.ID_PEOPLE_2))).thenReturn(Optional.of(PeopleMock.CreatePeople2()));

		String json = getJSON(PeopleInputDtoMock.CreatePeople2());

		mockMvc.perform(delete(BASE_URL + "/people/" + PeopleData.ID_PEOPLE_2)
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}

	private String getJSON(PeopleInputDto dto) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(dto);
	}
}