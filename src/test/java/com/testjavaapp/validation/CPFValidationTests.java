package com.testjavaapp.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.testjavaapp.mock.data.PeopleData;

public class CPFValidationTests {

	private CPFValidation validator;

	@BeforeEach
	void setup() {
		validator = new CPFValidation();
	}

	@Test
	@DisplayName("[CPFValidation.isValid] Must be a valid CPF.")
	public void mustValidCPF() {
		assertTrue(validator.isValid(PeopleData.DOCUMENT_PEOPLE_2));
	}

	@Test
	@DisplayName("[CPFValidation.isValid] Must be a invalid CPF when CPF is 44444444444.")
	public void mustInvalidModelWhenCPFIs44444444444() {
		assertFalse(validator.isValid("444.444.444-44"));
	}

	@Test
	@DisplayName("[CPFValidation.isValid] Must be a invalid CPF when CPF is incorrect size.")
	public void mustInvalidModelWhenCPFIsIncorrectSize() {
		assertFalse(validator.isValid("444.444.444"));
	}

	@Test
	@DisplayName("[CPFValidation.isValid] Must be a invalid CPF when invalid first check digit.")
	public void mustInvalidCPFWhenInvalidFirstCheckDigit() {
		assertFalse(validator.isValid(PeopleData.DOCUMENT_INVALID_FIRST_DIGIT));
	}

	@Test
	@DisplayName("[CPFValidation.isValid] Must be a invalid CPF when invalid second check digit.")
	public void mustInvalidCPFWhenInvalidSecondCheckDigit() {
		assertFalse(validator.isValid(PeopleData.DOCUMENT_INVALID_SECOND_DIGIT));
	}
}