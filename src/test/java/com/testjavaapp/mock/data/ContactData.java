package com.testjavaapp.mock.data;

public final class ContactData {
	public static final long ID_CONTACT_1 = 5482;
	public static final long ID_CONTACT_2 = 5483;

	public static final String NAME_CONTACT_1 = "Stella Liz Daiane Ferreira";
	public static final String EMAIL_CONTACT_1 = "sstellalizdaianeferreira@rafaelsouza.com.br";
	public static final String PHONE_CONTACT_1 = "(84) 3936-8024";
	public static final String NAME_CONTACT_2 = "Tatiane Jaqueline Vieira";
	public static final String EMAIL_CONTACT_2 = "ttatianejaquelinevieira@piemme.com.br";
	public static final String PHONE_CONTACT_2 = "(81) 2862-3449";
	
	public static final String PHONE_CONTACT_INCORRET_FORMAT_AND_SIZE = "(81)2862-3449";
	public static final String EMAIL_CONTACT_INCORRET_FORMAT_AND_SIZE = "me@woman";
}