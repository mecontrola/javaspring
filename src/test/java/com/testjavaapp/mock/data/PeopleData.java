package com.testjavaapp.mock.data;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public final class PeopleData {
	public static final long ID_PEOPLE_1 = 8745;
	public static final long ID_PEOPLE_2 = 8746;

	public static final String NAME_PEOPLE_1 = "Antonio Matheus Pinto";
	public static final String DOCUMENT_PEOPLE_1 = "729.534.351-23";
	public static final String NAME_PEOPLE_2 = "Giovanni Enrico Galvão";
	public static final String DOCUMENT_PEOPLE_2 = "038.274.000-90";

	public static final Date BORNDATE_PEOPLE_1 = Date.from(LocalDate.of(1994, 8, 19).atStartOfDay(ZoneId.systemDefault()).toInstant());
	public static final Date BORNDATE_PEOPLE_2 = Date.from(LocalDate.of(1992, 3, 23).atStartOfDay(ZoneId.systemDefault()).toInstant());
	
	public static final String DOCUMENT_INVALID_FIRST_DIGIT = "729.534.351-33";
	public static final String DOCUMENT_INVALID_SECOND_DIGIT = "729.534.351-25";
}