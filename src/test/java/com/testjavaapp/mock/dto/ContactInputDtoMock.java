package com.testjavaapp.mock.dto;

import com.testjavaapp.dto.ContactInputDto;
import com.testjavaapp.mock.data.ContactData;

public class ContactInputDtoMock {
	public static ContactInputDto CreateContact1() {
		return new ContactInputDto()
				.setName(ContactData.NAME_CONTACT_1)
				.setEmail(ContactData.EMAIL_CONTACT_1)
				.setPhone(ContactData.PHONE_CONTACT_1);
	}

	public static ContactInputDto CreateContact2() {
		return new ContactInputDto()
				.setName(ContactData.NAME_CONTACT_2)
				.setEmail(ContactData.EMAIL_CONTACT_2)
				.setPhone(ContactData.PHONE_CONTACT_2);
	}
}