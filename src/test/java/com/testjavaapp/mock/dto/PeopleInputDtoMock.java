package com.testjavaapp.mock.dto;

import com.testjavaapp.dto.PeopleInputDto;
import com.testjavaapp.mock.data.PeopleData;

public class PeopleInputDtoMock {
	public static PeopleInputDto CreatePeople1() {
		return new PeopleInputDto()
				.setName(PeopleData.NAME_PEOPLE_1)
				.setDocument(PeopleData.DOCUMENT_PEOPLE_1)
				.setBornDate(PeopleData.BORNDATE_PEOPLE_1)
				.addContact(ContactInputDtoMock.CreateContact1());
	}
	
	public static PeopleInputDto CreatePeople2() {
		return new PeopleInputDto()
				.setName(PeopleData.NAME_PEOPLE_2)
				.setDocument(PeopleData.DOCUMENT_PEOPLE_2)
				.setBornDate(PeopleData.BORNDATE_PEOPLE_2)
				.addContact(ContactInputDtoMock.CreateContact2());
	}
}