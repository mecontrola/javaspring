package com.testjavaapp.mock.model;

import com.testjavaapp.mock.data.ContactData;
import com.testjavaapp.model.Contact;

public class ContactMock {
	public static Contact CreateContact1() {
		return new Contact()
				.setId(ContactData.ID_CONTACT_1)
				.setName(ContactData.NAME_CONTACT_1)
				.setEmail(ContactData.EMAIL_CONTACT_1)
				.setPhone(ContactData.PHONE_CONTACT_1);
	}

	public static Contact CreateContact2() {
		return new Contact()
				.setId(ContactData.ID_CONTACT_2)
				.setName(ContactData.NAME_CONTACT_2)
				.setEmail(ContactData.EMAIL_CONTACT_2)
				.setPhone(ContactData.PHONE_CONTACT_2);
	}
}