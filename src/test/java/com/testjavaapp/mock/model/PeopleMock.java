package com.testjavaapp.mock.model;

import com.testjavaapp.mock.data.PeopleData;
import com.testjavaapp.model.People;

public class PeopleMock {
	public static People CreatePeople1() {
		return new People()
				.setId(PeopleData.ID_PEOPLE_1)
				.setName(PeopleData.NAME_PEOPLE_1)
				.setDocument(PeopleData.DOCUMENT_PEOPLE_1)
				.setBornDate(PeopleData.BORNDATE_PEOPLE_1)
				.addContact(ContactMock.CreateContact1());
	}

	public static People CreatePeople2() {
		return new People()
				.setId(PeopleData.ID_PEOPLE_2)
				.setName(PeopleData.NAME_PEOPLE_2)
				.setDocument(PeopleData.DOCUMENT_PEOPLE_2)
				.setBornDate(PeopleData.BORNDATE_PEOPLE_2)
				.addContact(ContactMock.CreateContact2());
	}
}