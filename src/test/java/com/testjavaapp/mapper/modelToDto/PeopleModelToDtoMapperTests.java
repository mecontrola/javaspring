package com.testjavaapp.mapper.modelToDto;

import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.testjavaapp.dto.PeopleDto;
import com.testjavaapp.helper.AssertHelper;
import com.testjavaapp.mock.model.PeopleMock;
import com.testjavaapp.model.People;

public class PeopleModelToDtoMapperTests {

	private PeopleModelToDtoMapper mapper;

	@BeforeEach
	void setup() {
		mapper = new PeopleModelToDtoMapper();
	}

	@Test
	@DisplayName("[PeopleModelToDtoMapper.toMap] Must convert an model to an DTO.")
	public void mustConvertModelToDto() {
		People actual = PeopleMock.CreatePeople1();
		PeopleDto expected = mapper.toMap(actual);

		AssertHelper.assertEqualsPeople(expected, actual);
	}

	@Test
	@DisplayName("[PeopleModelToDtoMapper.toMap] Must convert an list of models to an list of DTOs.")
	public void mustConvertListModelToListDto() {
		List<People> actual = new ArrayList<>();
		actual.add(PeopleMock.CreatePeople1());
		List<PeopleDto> expected = mapper.toMap(actual);

		AssertHelper.assertEqualsListPeopleDto(expected, actual);
	}

	@Test
	@DisplayName("[PeopleModelToDtoMapper.toMap] Must convert an null model to an null DTO.")
	public void mustConvertNullModelToNullDto() {
		People actual = null;
		PeopleDto expected = mapper.toMap(actual);

		assertNull(expected);
	}

	@Test
	@DisplayName("[PeopleModelToDtoMapper.toMap] Must convert an null list of models to an null list of DTOs.")
	public void mustConvertNullListModelToNullListDto() {
		List<People> actual = null;
		List<PeopleDto> expected = mapper.toMap(actual);

		assertNull(expected);
	}
}