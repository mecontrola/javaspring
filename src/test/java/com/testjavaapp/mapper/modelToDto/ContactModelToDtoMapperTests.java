package com.testjavaapp.mapper.modelToDto;

import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.testjavaapp.dto.ContactDto;
import com.testjavaapp.helper.AssertHelper;
import com.testjavaapp.mock.model.ContactMock;
import com.testjavaapp.model.Contact;

public class ContactModelToDtoMapperTests {

	private ContactModelToDtoMapper mapper;

	@BeforeEach
	void setup() {
		mapper = new ContactModelToDtoMapper();
	}

	@Test
	@DisplayName("[ContactModelToDtoMapper.toMap] Must convert an model to an DTO.")
	public void mustConvertInputDtoToEntity() {
		Contact actual = ContactMock.CreateContact1();
		ContactDto expected = mapper.toMap(actual);

		AssertHelper.assertEqualsContact(expected, actual);
	}

	@Test
	@DisplayName("[ContactModelToDtoMapper.toMap] Must convert an list of models to an list of DTOs.")
	public void mustConvertListModelToListDto() {
		List<Contact> actual = new ArrayList<>();
		actual.add(ContactMock.CreateContact1());
		List<ContactDto> expected = mapper.toMap(actual);

		AssertHelper.assertEqualsListContactDto(expected, actual);
	}

	@Test
	@DisplayName("[ContactModelToDtoMapper.toMap] Must convert an null model to an null DTO.")
	public void mustConvertModelNullToDto() {
		Contact actual = null;
		ContactDto expected = mapper.toMap(actual);

		assertNull(expected);
	}

	@Test
	@DisplayName("[ContactModelToDtoMapper.toMap] Must convert an null list of models to an null list of DTOs.")
	public void mustConvertNullListModelToNullListDto() {
		List<Contact> actual = null;
		List<ContactDto> expected = mapper.toMap(actual);

		assertNull(expected);
	}
}