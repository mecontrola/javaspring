package com.testjavaapp.mapper.inputDtoToModel;

import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.testjavaapp.dto.ContactInputDto;
import com.testjavaapp.helper.AssertHelper;
import com.testjavaapp.mock.dto.ContactInputDtoMock;
import com.testjavaapp.model.Contact;

public class ContactInputDtoToModelMapperTests {

	private ContactInputDtoToModelMapper mapper;

	@BeforeEach
	void setup() {
		mapper = new ContactInputDtoToModelMapper();
	}

	@Test
	@DisplayName("[ContactInputDtoToModelMapper.toMap] Must convert an input DTO to an model.")
	public void mustConvertInputDtoToModel() {
		ContactInputDto actual = ContactInputDtoMock.CreateContact1();
		Contact expected = mapper.toMap(actual);

		AssertHelper.assertEqualsContact(expected, actual);
	}

	@Test
	@DisplayName("[ContactInputDtoToModelMapper.toMap] Must convert an list of input DTOs to an list of models.")
	public void mustConvertListInputDtoToListModel() {
		List<ContactInputDto> actual = new ArrayList<>();
		actual.add(ContactInputDtoMock.CreateContact1());
		List<Contact> expected = mapper.toMap(actual);

		AssertHelper.assertEqualsListContact(expected, actual);
	}

	@Test
	@DisplayName("[ContactInputDtoToModelMapper.toMap] Must convert an null input DTO to an null model.")
	public void mustConvertInputDtoNullToModel() {
		ContactInputDto actual = null;
		Contact expected = mapper.toMap(actual);

		assertNull(expected);
	}

	@Test
	@DisplayName("[ContactInputDtoToModelMapper.toMap] Must convert an null list of input DTOs to an null list of models.")
	public void mustConvertNullListModelToNullListDto() {
		List<ContactInputDto> actual = null;
		List<Contact> expected = mapper.toMap(actual);

		assertNull(expected);
	}
}