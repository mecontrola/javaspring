package com.testjavaapp.mapper.inputDtoToModel;

import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.testjavaapp.dto.PeopleInputDto;
import com.testjavaapp.helper.AssertHelper;
import com.testjavaapp.mock.dto.PeopleInputDtoMock;
import com.testjavaapp.model.People;

public class PeopleInputDtoToModelMapperTests {

	private PeopleInputDtoToModelMapper mapper;

	@BeforeEach
	void setup() {
		mapper = new PeopleInputDtoToModelMapper();
	}

	@Test
	@DisplayName("[PeopleInputDtoToModelMapper.toMap] Must convert an input DTO to an model.")
	public void mustConvertInputDtoToModel() {
		PeopleInputDto actual = PeopleInputDtoMock.CreatePeople1();
		People expected = mapper.toMap(actual);

		AssertHelper.assertEqualsPeople(expected, actual);
	}

	@Test
	@DisplayName("[PeopleInputDtoToModelMapper.toMap] Must convert an list of input DTOs to an list of models.")
	public void mustConvertListInputDtoToListModel() {
		List<PeopleInputDto> actual = new ArrayList<>();
		actual.add(PeopleInputDtoMock.CreatePeople1());
		List<People> expected = mapper.toMap(actual);

		AssertHelper.assertEqualsListPeople(expected, actual);
	}

	@Test
	@DisplayName("[PeopleInputDtoToModelMapper.toMap] Must convert an null input DTO to an null model.")
	public void mustConvertInputDtoNullToModel() {
		PeopleInputDto actual = null;
		People expected = mapper.toMap(actual);

		assertNull(expected);
	}

	@Test
	@DisplayName("[PeopleInputDtoToModelMapper.toMap] Must convert an null list of input DTOs to an null list of models.")
	public void mustConvertNullListModelToNullListDto() {
		List<PeopleInputDto> actual = null;
		List<People> expected = mapper.toMap(actual);

		assertNull(expected);
	}
}