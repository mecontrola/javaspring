package com.testjavaapp.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.testjavaapp.dto.ContactInputDto;
import com.testjavaapp.mock.data.ContactData;
import com.testjavaapp.mock.dto.ContactInputDtoMock;

public class ContactInputDtoValidationTests {

	private Validator validator;

	@BeforeEach
	void setup() {
		validator = Validation.buildDefaultValidatorFactory().getValidator();
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a valid input dto.")
	public void mustValidInputDto() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertTrue(violations.isEmpty());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when null name.")
	public void mustInvalidInputDtoWhenNullName() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setName(null);

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when empty name.")
	public void mustInvalidInputDtoWhenEmptyName() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setName("");

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when blank name.")
	public void mustInvalidInputDtoWhenBlankName() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setName(" ");

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when name is longer than allowed.")
	public void mustInvalidInputDtoWhenLargerSizeName() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();

		while (dto.getName().length() < 150)
			dto.setName(dto.getName() + ContactData.NAME_CONTACT_1);

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when null phone.")
	public void mustInvalidInputDtoWhenNullPhone() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setPhone(null);

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when empty phone.")
	public void mustInvalidInputDtoWhenEmptyPhone() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setPhone("");

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(2, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when blank phone.")
	public void mustInvalidInputDtoWhenBlankPhone() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setPhone(" ");

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(2, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when phone is longer than allowed.")
	public void mustInvalidInputDtoWhenLargerSizePhone() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setPhone(ContactData.PHONE_CONTACT_INCORRET_FORMAT_AND_SIZE);

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when phone not formatted.")
	public void mustInvalidInputDtoWhenNotFormattedPhone() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setPhone(ContactData.PHONE_CONTACT_INCORRET_FORMAT_AND_SIZE);

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when null email.")
	public void mustInvalidInputDtoWhenNullEmail() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setEmail(null);

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when empty email.")
	public void mustInvalidInputDtoWhenEmptyEmail() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setEmail("");

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(2, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when blank email.")
	public void mustInvalidInputDtoWhenBlankEmail() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setEmail(" ");

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(3, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when email is longer than allowed.")
	public void mustInvalidInputDtoWhenLargerSizeEmail() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setEmail(ContactData.EMAIL_CONTACT_INCORRET_FORMAT_AND_SIZE);

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}

	@Test
	@DisplayName("[ContactInputDto] Must be a invalid input dto when email not formatted.")
	public void mustInvalidInputDtoWhenNotFormattedEmail() {
		ContactInputDto dto = ContactInputDtoMock.CreateContact1();
		dto.setEmail(ContactData.EMAIL_CONTACT_INCORRET_FORMAT_AND_SIZE);

		Set<ConstraintViolation<ContactInputDto>> violations = validator.validate(dto);

		assertEquals(1, violations.size());
	}
}